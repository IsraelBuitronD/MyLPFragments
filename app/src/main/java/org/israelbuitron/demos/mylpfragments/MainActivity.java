package org.israelbuitron.demos.mylpfragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import org.israelbuitron.demos.mylpfragments.fragments.LandscapeFragment;
import org.israelbuitron.demos.mylpfragments.fragments.PortraitFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment f = null;

        Configuration c = getResources().getConfiguration();

        if (c.orientation == Configuration.ORIENTATION_PORTRAIT) {
            f = (Fragment) new PortraitFragment();
            ft.replace(android.R.id.content, f);
        } else {
            f = (Fragment) new LandscapeFragment();
            ft.replace(android.R.id.content, f);
        }

        ft.commit();
    }
}
